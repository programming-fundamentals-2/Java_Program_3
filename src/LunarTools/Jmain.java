package LunarTools;

import java.text.DecimalFormat;
import java.util.Calendar;

import javax.swing.JOptionPane;

public class Jmain {

	
	public static void main(String[] args) {
	////////////////////////
	//implementation here!//
	////////////////////////
		
		//init vars
		int givenYear = 0; 
		
		//ask user to input a year
		String yearInput= JOptionPane.showInputDialog("Please input a year in the format (YYYY)");
		givenYear = Integer.parseInt(yearInput); //convert user input to int
		
		
		
		//create objects w/ previously input year
			// Moon object
			moon m1 = new moon(givenYear); // constructor w/ user input
		
			// Curiousity object
			curiousity c1 = new curiousity(givenYear); // constructor w/ user input

			
		//display output to user (Java Info Pane?)
			//Dispay lunar distance info (moon class)
			m1.LunarStatus(); //void method prints status message in console
			
			//Display curiousity info (curiousity class)
			c1.RoverStatus(); //void method prints status message in console
	}

}
