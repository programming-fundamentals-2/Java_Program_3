package LunarTools;

import java.util.Calendar;

public class curiousity {
	
	//1. GET THE YEAR FROM USER INPUT (PARAMETER FROM CONTRUCTOR)
		//1A. CREATE A CUSTOM CONSTRUCTOR THAT USES THE CURRENT YEAR FROM JAVA CALENDAR LIB.
	//2. FIND OUT IF CURIOUSITY HAS LANDED. [METHOD]
		//2A. BOOL... (IF YEAR > 2012 == TRUE)
	//3. IF CURIOUSITY HAS LANDED HOW LONG FOR (YEAR-2012) [METHOD]
	//4. IF CURIOUSITY HAS NOT LANDED, HOW LONG UNTIL LANDING (2012-YEAR) [METHOD]

	
	//init vars
	private int year;
	//get current year
	private int curyear = Calendar.getInstance().get(Calendar.YEAR);
	
	
	//default constructor (no paramaters, uses current year)
	public curiousity(){
		year = curyear;//current year
	}
	//custom constructor (takes a year parameter, format YYYY)	
	public curiousity(int y){
		year = y ; //supplied year
	}
	
	private boolean hasLanded(){
		if (year < 2012 ){
			return false;
		}
		else return true;
	}
	
	//return and print the status of the rover. 
	//Void funtion to print defined text for consistency in implementation.
	public void RoverStatus(){
		//has not landed yet
		if (!hasLanded()){
			System.out.println("The \"Curiousity\" mars rover will be landing in " + (2012-year) + " year(s)");
			}
		//has landed
		else {
			System.out.println("The \"Curiousity\" rover has been on mars for " + (year-2012) + " year(s)");
			}
	}
	
}
