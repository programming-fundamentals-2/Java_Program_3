package LunarTools;
import java.text.DecimalFormat;
import java.util.Calendar;


public class moon {
		
		private int year;
		private final double distance = 252E3; //average distance moon is from earth in miles
		private final static double rate = 2.3612E-5; // 3.8 cm converted to miles //movement away in cm / year.
		private static double newdistance;
		private static double shortmarsyears;
		private static double greatmarsyears;
		private static double shortmars = 545E5;
		private static double greatmars = 4013E5;
		private final static double marsrate = 3.8E-5;
		
		//init decimal format
		DecimalFormat df = new DecimalFormat("#.##");
		
		//get current year
		private int curyear = Calendar.getInstance().get(Calendar.YEAR);
		
		//default constructor (no paramaters, uses current year)
		public moon(){
			year = curyear;//current year
			newdistance = 0;//init distance 
		}
		//custom constructor (takes a year parameter, format YYYY)	
		public moon(int y){
			year = y ; //supplied year
			newdistance = 0;//init distance 
		}
		
		//convert miles to cm
		
		public String getMarsYear(){
			//Shortest 54,500,000 kilometers 545E5
			//Greatest 401,300,000 kilometers 4013E5
			//3.8 cm - 3.8E-5 kilometers
			String mars;
			shortmarsyears = ((shortmars / marsrate) - 405554.688);
			greatmarsyears = ((greatmars / marsrate) - 405554.688);
			mars = Double.toString(shortmarsyears) + " or " + Double.toString(greatmarsyears); 
			return mars;
		}
		
		public double getDistance(){
			//calc the new distance
			newdistance = (rate * (year - curyear)) + distance;
			return newdistance;	
		}
		
		//void method to write the status message, for consistency
		public void LunarStatus(){
			System.out.println("The current distance of the moon in relation to mars is: " + df.format(getDistance()) + " (km)");
		}
	}