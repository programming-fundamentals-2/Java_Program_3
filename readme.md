# Java Program #3 Add to Moon Class

**Student:** Liam Hockley

**Submitted Date:** Mar 19, 2012 4:53 pm

**Grade:** 30.0 (max 30.0)

**Instructions**

> New class to your Moon Program:
> Curiosity is a new addition to the Mars rovers.  It will land on Mars on Aug 6th, 2012. 
> 
> Here is the NASA site with more information:
> 
> http://www.nasa.gov/mission_pages/msl/index.html
> 
> You are going to add a new class to your Moon program – called Curiosity Class.
> 
> This class is going to use the information found on this site to tell the user just how > far (in kilometers) Curiosity has to go to Mars before landing.  Keep your calculations > in days although their clock is ticking down in seconds.  The graphic in the middle of the page gives you the details in kilometers.
> 
> Devise your calculations based on the day you look at the site – that is – you don’t have to create a real-time situation – just use round numbers based on your assumptions when you gather the data.
> 
> The user will provide the program with a date and you will display the number of kilometers yet to go before landing on Mars.  If they give you the day of landing – Aug 6th or later – you will tell them that Curiosity has landed and is exploring the surface for signs of life – maybe it will find life J!
> 
> Make sure you add a new object to your Implementation file so that the user can make a selection to see Curiosity’s timeline as well as your Moon class objects.
> 
> Use your own Moon code or if your code was incomplete – I have added my code to the resources link under Assignment Solutions – MoonClassSolution.
> 
> Let me or the tutor know if you have any questions.